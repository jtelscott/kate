# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Danishka Navin <danishka@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-29 00:48+0000\n"
"PO-Revision-Date: 2011-08-09 05:54+0530\n"
"Last-Translator: Danishka Navin <danishka@gmail.com>\n"
"Language-Team: Sinhala <info@hanthana.org>\n"
"Language: si\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 1.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Danishka Navin"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "danishka@gmail.com"

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr "ප්‍රථිදානය"

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#: build.ui:56
#, fuzzy, kde-format
#| msgid "Build Plugin"
msgid "Build again"
msgstr "Build ප්ලගිනය"

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#: build.ui:63
#, kde-format
msgid "Cancel"
msgstr ""

#: buildconfig.cpp:26
#, kde-format
msgid "Add errors and warnings to Diagnostics"
msgstr ""

#: buildconfig.cpp:37
#, fuzzy, kde-format
#| msgid "Build Plugin"
msgid "Build & Run"
msgstr "Build ප්ලගිනය"

#: buildconfig.cpp:43
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Build & Run Settings"
msgstr "ගොඩනැගීම අසාර්ථකයි."

#: plugin_katebuild.cpp:209 plugin_katebuild.cpp:216 plugin_katebuild.cpp:1200
#, kde-format
msgid "Build"
msgstr "Build"

#: plugin_katebuild.cpp:219
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Select Target..."
msgstr "ගොඩනැගීම අසාර්ථකයි."

#: plugin_katebuild.cpp:224
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Build Selected Target"
msgstr "ගොඩනැගීම අසාර්ථකයි."

#: plugin_katebuild.cpp:229
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Build and Run Selected Target"
msgstr "ගොඩනැගීම අසාර්ථකයි."

#: plugin_katebuild.cpp:234
#, kde-format
msgid "Stop"
msgstr "නවතන්න"

#: plugin_katebuild.cpp:239
#, kde-format
msgctxt "Left is also left in RTL mode"
msgid "Focus Next Tab to the Left"
msgstr ""

#: plugin_katebuild.cpp:259
#, kde-format
msgctxt "Right is right also in RTL mode"
msgid "Focus Next Tab to the Right"
msgstr ""

#: plugin_katebuild.cpp:281
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr "ඉලක්ක සැකසුම්"

#: plugin_katebuild.cpp:400
#, fuzzy, kde-format
#| msgid "Build Plugin"
msgid "Build Information"
msgstr "Build ප්ලගිනය"

#: plugin_katebuild.cpp:487 plugin_katebuild.cpp:1253 plugin_katebuild.cpp:1264
#: plugin_katebuild.cpp:1285 plugin_katebuild.cpp:1295
#, kde-format
msgid "Project Plugin Targets"
msgstr ""

#: plugin_katebuild.cpp:598
#, kde-format
msgid "There is no file or directory specified for building."
msgstr "තැනීම ගොඩනැගීම"

#: plugin_katebuild.cpp:602
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr "\"%1\" ගොනුව දේශීය ගොනුවක් නොවේ. දේශීය-නොවන ගොනුව සම්පාදනය කල නොහැක."

#: plugin_katebuild.cpp:649
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""

#: plugin_katebuild.cpp:663
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "\"%1\" ධාවනය අසාර්ථක විය. පිටවීම්තත්වය = %2"

#: plugin_katebuild.cpp:678
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr ""

#: plugin_katebuild.cpp:785
#, kde-format
msgid "No target available for building."
msgstr ""

#: plugin_katebuild.cpp:799
#, fuzzy, kde-format
#| msgid "There is no file or directory specified for building."
msgid "There is no local file or directory specified for building."
msgstr "තැනීම ගොඩනැගීම"

#: plugin_katebuild.cpp:805
#, kde-format
msgid "Already building..."
msgstr ""

#: plugin_katebuild.cpp:832
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Building target <b>%1</b> ..."
msgstr "ගොඩනැගීම අසාර්ථකයි."

#: plugin_katebuild.cpp:846
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr ""

#: plugin_katebuild.cpp:882
#, kde-format
msgid "Build <b>%1</b> completed. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:888
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] "එක් දෝශයක් හමුවිය"
msgstr[1] "දෝශ %1 හමුවිය"

#: plugin_katebuild.cpp:892
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] "එක් අවවාදයක් හමුවිය"
msgstr[1] "අවවාද %1ක් හමුවිය"

#: plugin_katebuild.cpp:895
#, fuzzy, kde-format
#| msgid "Found one error."
#| msgid_plural "Found %1 errors."
msgid "Found one note."
msgid_plural "Found %1 notes."
msgstr[0] "එක් දෝශයක් හමුවිය"
msgstr[1] "දෝශ %1 හමුවිය"

#: plugin_katebuild.cpp:900
#, kde-format
msgid "Build failed."
msgstr "ගොඩනැගීම අසාර්ථකයි."

#: plugin_katebuild.cpp:902
#, kde-format
msgid "Build completed without problems."
msgstr "දෝශයකින් තොරව ගොඩනැගීම සම්පූර්ණ විය."

#: plugin_katebuild.cpp:907
#, kde-format
msgid "Build <b>%1 canceled</b>. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:931
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr ""

#: plugin_katebuild.cpp:1157
#, fuzzy, kde-format
#| msgctxt "The same word as 'make' uses to mark an error."
#| msgid "error"
msgctxt "The same word as 'gcc' uses for an error."
msgid "error"
msgstr "දෝෂය"

#: plugin_katebuild.cpp:1160
#, fuzzy, kde-format
#| msgctxt "The same word as 'make' uses to mark a warning."
#| msgid "warning"
msgctxt "The same word as 'gcc' uses for a warning."
msgid "warning"
msgstr "අවවාදය"

#: plugin_katebuild.cpp:1163
#, kde-format
msgctxt "The same words as 'gcc' uses for note or info."
msgid "note|info"
msgstr ""

#: plugin_katebuild.cpp:1166
#, kde-format
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr "නොදෙන ලද යොමුව"

#: plugin_katebuild.cpp:1199
#, fuzzy, kde-format
#| msgid "Targets"
msgid "Target Set"
msgstr "ඉලක්ක"

#: plugin_katebuild.cpp:1201
#, kde-format
msgid "Clean"
msgstr "පිරිසිදු කරන්න"

#: plugin_katebuild.cpp:1202
#, kde-format
msgid "Config"
msgstr "සකසන්න"

#: plugin_katebuild.cpp:1203
#, fuzzy, kde-format
#| msgid "Config"
msgid "ConfigClean"
msgstr "සකසන්න"

#: plugin_katebuild.cpp:1323
#, fuzzy, kde-format
#| msgid "Build"
msgid "build"
msgstr "Build"

#: plugin_katebuild.cpp:1326
#, fuzzy, kde-format
#| msgid "Clean"
msgid "clean"
msgstr "පිරිසිදු කරන්න"

#: plugin_katebuild.cpp:1329
#, kde-format
msgid "quick"
msgstr ""

#: TargetHtmlDelegate.cpp:47
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr ""

#: TargetHtmlDelegate.cpp:49
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr ""

#: TargetHtmlDelegate.cpp:98
#, fuzzy, kde-format
#| msgid "Leave empty to use the directory of the current document. "
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr "වත්මන් ලේඛනය සඳහා බහලුම යොදාගැනීමට එය හිස්ව තබන්න."

#: TargetHtmlDelegate.cpp:102
#, fuzzy, kde-format
#| msgid ""
#| "Use:\n"
#| "\"%f\" for current file\n"
#| "\"%d\" for directory of current file"
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""
"භාවිතය:\n"
"වත්මන් ලේඛනය සඳහා \"%f\"\n"
"වත්මන් ගොනුවේ බහාලුම සඳහා \"%d\""

#: TargetModel.cpp:388
#, kde-format
msgid "Command/Target-set Name"
msgstr ""

#: TargetModel.cpp:391
#, fuzzy, kde-format
#| msgid "Working directory"
msgid "Working Directory / Command"
msgstr "ක්‍රියාකරන නාමාවලිය"

#: TargetModel.cpp:394
#, kde-format
msgid "Run Command"
msgstr ""

#: targets.cpp:23
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr ""

#: targets.cpp:27
#, kde-format
msgid "Create new set of targets"
msgstr ""

#: targets.cpp:31
#, kde-format
msgid "Copy command or target set"
msgstr ""

#: targets.cpp:35
#, kde-format
msgid "Delete current target or current set of targets"
msgstr ""

#: targets.cpp:40
#, kde-format
msgid "Add new target"
msgstr ""

#: targets.cpp:44
#, kde-format
msgid "Build selected target"
msgstr ""

#: targets.cpp:48
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Build and run selected target"
msgstr "ගොඩනැගීම අසාර්ථකයි."

#: targets.cpp:52
#, fuzzy, kde-format
#| msgid "Build failed."
msgid "Move selected target up"
msgstr "ගොඩනැගීම අසාර්ථකයි."

#: targets.cpp:56
#, kde-format
msgid "Move selected target down"
msgstr ""

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr "ගොඩනැගීම (&B)"

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr ""

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr ""

#, fuzzy
#~| msgid "Build failed."
#~ msgid "Building <b>%1</b> had warnings."
#~ msgstr "ගොඩනැගීම අසාර්ථකයි."

#~ msgctxt "Header for the file name column"
#~ msgid "File"
#~ msgstr "ගොනුව"

#~ msgctxt "Header for the line number column"
#~ msgid "Line"
#~ msgstr "රේඛාව"

#~ msgctxt "Header for the error message column"
#~ msgid "Message"
#~ msgstr "පණිවුඩය"

#~ msgid "Next Error"
#~ msgstr "ඊළඟ දෝශය"

#~ msgid "Previous Error"
#~ msgstr "පෙර දෝශය"

#, fuzzy
#~| msgctxt "The same word as 'make' uses to mark an error."
#~| msgid "error"
#~ msgid "Error"
#~ msgstr "දෝෂය"

#, fuzzy
#~| msgctxt "The same word as 'make' uses to mark a warning."
#~| msgid "warning"
#~ msgid "Warning"
#~ msgstr "අවවාදය"

#, fuzzy
#~| msgctxt "The same word as 'make' uses to mark an error."
#~| msgid "error"
#~ msgid "Only Errors"
#~ msgstr "දෝෂය"

#, fuzzy
#~| msgid "Errors && Warnings"
#~ msgid "Errors and Warnings"
#~ msgstr "දෝශ සහ අවවාද"

#, fuzzy
#~| msgid "Build Output"
#~ msgid "Parsed Output"
#~ msgstr "Build ප්‍රතිදානය"

#, fuzzy
#~| msgid "Build Output"
#~ msgid "Full Output"
#~ msgstr "Build ප්‍රතිදානය"

#, fuzzy
#~| msgid "Build failed."
#~ msgid "Build and Run Default Target"
#~ msgstr "ගොඩනැගීම අසාර්ථකයි."

#, fuzzy
#~| msgid "Build failed."
#~ msgid "Build Previous Target"
#~ msgstr "ගොඩනැගීම අසාර්ථකයි."

#, fuzzy
#~| msgid "Config"
#~ msgid "config"
#~ msgstr "සකසන්න"

#, fuzzy
#~| msgid "Build Plugin"
#~ msgid "Kate Build Plugin"
#~ msgstr "Build ප්ලගිනය"

#~ msgid "Build Output"
#~ msgstr "Build ප්‍රතිදානය"

#, fuzzy
#~| msgid "Next Target"
#~ msgid "Next Set of Targets"
#~ msgstr "ඊලඟ ඉලක්කය"

#, fuzzy
#~| msgid "Target %1"
#~ msgid "Target Set %1"
#~ msgstr "ඉලක්කය %1"

#, fuzzy
#~| msgid "Targets"
#~ msgid "Target"
#~ msgstr "ඉලක්ක"

#, fuzzy
#~| msgid "Targets"
#~ msgid "Target:"
#~ msgstr "ඉලක්ක"

#, fuzzy
#~| msgid "Next Target"
#~ msgid "Sets of Targets"
#~ msgstr "ඊලඟ ඉලක්කය"

#~ msgid "Make Results"
#~ msgstr "ප්‍රථිපල සකසන්න"

#~ msgid "Quick Compile"
#~ msgstr "ක්‍ෂණික සම්පාදනය"

#~ msgid "The custom command is empty."
#~ msgstr "රුචි විධානය හිස්."

#~ msgid "New"
#~ msgstr "නව"

#~ msgid "Copy"
#~ msgstr "පිටපත් කරන්න"

#~ msgid "Delete"
#~ msgstr "මකන්න"

#~ msgid "Quick compile"
#~ msgstr "ක්‍ෂණික සම්පාදනය"
