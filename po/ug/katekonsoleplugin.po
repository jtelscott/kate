# Uyghur translation for katekonsoleplugin.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sahran <sahran.ug@gmail.com>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: katekonsoleplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-09 00:51+0000\n"
"PO-Revision-Date: 2013-09-08 07:04+0900\n"
"Last-Translator: Gheyret Kenji <gheyret@gmail.com>\n"
"Language-Team: Uyghur Computer Science Association <UKIJ@yahoogroups.com>\n"
"Language: ug\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: kateconsole.cpp:56
#, kde-format
msgid "You do not have enough karma to access a shell or terminal emulation"
msgstr ""
"سىزنىڭ shell ياكى تېرمىنال تەقلىدلىگۈچنى زىيارەت قىلىشقا يېتەرلىك ھوقۇقىڭىز "
"يوق"

#: kateconsole.cpp:104 kateconsole.cpp:134 kateconsole.cpp:664
#, kde-format
msgid "Terminal"
msgstr "تېرمىنال"

#: kateconsole.cpp:143
#, kde-format
msgctxt "@action"
msgid "&Pipe to Terminal"
msgstr "تۇرۇبىدىن تېرمىنالغا(&P)"

#: kateconsole.cpp:147
#, kde-format
msgctxt "@action"
msgid "S&ynchronize Terminal with Current Document"
msgstr "نۆۋەتتىكى پۈتۈك بىلەن تېرمىنالنى قەدەمداشلا(&Y)"

#: kateconsole.cpp:151
#, kde-format
msgctxt "@action"
msgid "Run Current Document"
msgstr ""

#: kateconsole.cpp:156 kateconsole.cpp:513
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "&Focus Terminal"
msgctxt "@action"
msgid "S&how Terminal Panel"
msgstr "تېرمىنال فوكۇسى(&F)"

#: kateconsole.cpp:162
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "&Focus Terminal"
msgctxt "@action"
msgid "&Focus Terminal Panel"
msgstr "تېرمىنال فوكۇسى(&F)"

#: kateconsole.cpp:307
#, kde-format
msgid ""
"Konsole not installed. Please install konsole to be able to use the terminal."
msgstr ""

#: kateconsole.cpp:388
#, kde-format
msgid ""
"Do you really want to pipe the text to the console? This will execute any "
"contained commands with your user rights."
msgstr ""
"سىز راستلا بۇ تېكىست تۇرۇبىسىنى تىزگىن تاختىغا كىرگۈزەمسىز؟ بۇنداق بولغاندا "
"ئىشلەتكۈچى ھوقۇقىڭىز مەۋجۇت ھەر قانداق بۇيرۇقنى ئىجرا قىلىشى مۇمكىن."

#: kateconsole.cpp:389
#, kde-format
msgid "Pipe to Terminal?"
msgstr "تۇرۇبىدىن تېرمىنالغا؟"

#: kateconsole.cpp:390
#, kde-format
msgid "Pipe to Terminal"
msgstr "تۇرۇبىدىن تېرمىنالغا"

#: kateconsole.cpp:418
#, fuzzy, kde-format
#| msgid "Sorry, can not cd into '%1'"
msgid "Sorry, cannot cd into '%1'"
msgstr "كەچۈرۈڭ، مۇندەرىجە ‹%1› غا كىرەلمەيدۇ"

#: kateconsole.cpp:454
#, kde-format
msgid "Not a local file: '%1'"
msgstr ""

#: kateconsole.cpp:487
#, fuzzy, kde-format
#| msgid ""
#| "Do you really want to pipe the text to the console? This will execute any "
#| "contained commands with your user rights."
msgid ""
"Do you really want to Run the document ?\n"
"This will execute the following command,\n"
"with your user rights, in the terminal:\n"
"'%1'"
msgstr ""
"سىز راستلا بۇ تېكىست تۇرۇبىسىنى تىزگىن تاختىغا كىرگۈزەمسىز؟ بۇنداق بولغاندا "
"ئىشلەتكۈچى ھوقۇقىڭىز مەۋجۇت ھەر قانداق بۇيرۇقنى ئىجرا قىلىشى مۇمكىن."

#: kateconsole.cpp:494
#, fuzzy, kde-format
#| msgid "Pipe to Terminal?"
msgid "Run in Terminal?"
msgstr "تۇرۇبىدىن تېرمىنالغا؟"

#: kateconsole.cpp:495
#, kde-format
msgid "Run"
msgstr ""

#: kateconsole.cpp:510
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "&Pipe to Terminal"
msgctxt "@action"
msgid "&Hide Terminal Panel"
msgstr "تۇرۇبىدىن تېرمىنالغا(&P)"

#: kateconsole.cpp:521
#, fuzzy, kde-format
#| msgid "Defocus Terminal"
msgid "Defocus Terminal Panel"
msgstr "تېرمىنالنى فوكۇسلىما"

#: kateconsole.cpp:522 kateconsole.cpp:523
#, fuzzy, kde-format
#| msgid "Focus Terminal"
msgid "Focus Terminal Panel"
msgstr "تېرمىنال فوكۇسى"

#: kateconsole.cpp:597
#, kde-format
msgid ""
"&Automatically synchronize the terminal with the current document when "
"possible"
msgstr ""
"مۇمكىن بولغاندا نۆۋەتتىكى پۈتۈك ئارقىلىق تېرمىنالنى ئۆزلۈكىدىن "
"قەدەمداشلايدۇ(&A)"

#: kateconsole.cpp:601 kateconsole.cpp:622
#, fuzzy, kde-format
#| msgid "Pipe to Terminal"
msgid "Run in terminal"
msgstr "تۇرۇبىدىن تېرمىنالغا"

#: kateconsole.cpp:603
#, kde-format
msgid "&Remove extension"
msgstr ""

#: kateconsole.cpp:608
#, kde-format
msgid "Prefix:"
msgstr ""

#: kateconsole.cpp:616
#, kde-format
msgid "&Show warning next time"
msgstr ""

#: kateconsole.cpp:618
#, kde-format
msgid ""
"The next time '%1' is executed, make sure a warning window will pop up, "
"displaying the command to be sent to terminal, for review."
msgstr ""

#: kateconsole.cpp:629
#, kde-format
msgid "Set &EDITOR environment variable to 'kate -b'"
msgstr ""
"بۇ 'kate -b' بۇيرۇقىغا نىسبەتەن &EDITOR مۇھىت ئۆزگەرگۈچى مىقدار تەڭشەيدۇ"

#: kateconsole.cpp:632
#, kde-format
msgid ""
"Important: The document has to be closed to make the console application "
"continue"
msgstr ""
"مۇھىم: يېپىلماقچى بولغان ھۆججەت تىزگىن تاختا پروگراممىنىڭ داۋاملاشتۇرىدۇ"

#: kateconsole.cpp:635
#, kde-format
msgid "Hide Konsole on pressing 'Esc'"
msgstr ""

#: kateconsole.cpp:638
#, kde-format
msgid ""
"This may cause issues with terminal apps that use Esc key, for e.g., vim. "
"Add these apps in the input below (Comma separated list)"
msgstr ""

#: kateconsole.cpp:669
#, kde-format
msgid "Terminal Settings"
msgstr "تېرمىنال تەڭشەك"

#. i18n: ectx: Menu (tools)
#: ui.rc:6
#, kde-format
msgid "&Tools"
msgstr "قوراللار(&T)"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "ئابدۇقادىر ئابلىز, غەيرەت كەنجى"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sahran.ug@gmail.com,  gheyret@gmail.com"

#, fuzzy
#~| msgid "Terminal"
#~ msgid "Kate Terminal"
#~ msgstr "تېرمىنال"

#, fuzzy
#~| msgid "Terminal"
#~ msgid "Terminal Panel"
#~ msgstr "تېرمىنال"

#~ msgid "Konsole"
#~ msgstr "Konsole"

#~ msgid "Embedded Konsole"
#~ msgstr "سىڭدۈرمە Konsole"
